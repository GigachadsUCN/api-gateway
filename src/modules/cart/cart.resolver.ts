import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Observable } from 'rxjs';
import { Cart, CartItem } from '../../types/graphql.schema'
import { CartMsg } from '../../utils/constants';
import { RabbitMQProxy } from '../../infrastructure/rabbitMQ/clientProxy';

@Resolver('Cart')
export class CartResolver {
  constructor(private readonly clientProxy: RabbitMQProxy) { }
  private _clientProxyCart = this.clientProxy.clientProxyCart();

  @Query('getCart')
  getCart(@Args('id') id: string): Observable<Cart> {
    return this._clientProxyCart.send(CartMsg.GET, id);
  }

  @Mutation('addCartItem')
  addCartItem(
    @Args('cartId') cartId: string,
    @Args('productId') productId: string,
    @Args('quantity') quantity: number,
  ): Observable<Cart> {
    return this._clientProxyCart.send(CartMsg.ADD, { cartId, productId, quantity });
  }

  @Mutation('deleteCartItem')
  deleteCartItem(
    @Args('cartId') cartId: string,
    @Args('productId') productId: string,
    @Args('quantity') quantity: number,
  ): Observable<Cart> {
    return this._clientProxyCart.send(CartMsg.DELETE, { cartId, productId, quantity });
  }
}
