import { Module } from '@nestjs/common';
import { ProxyModule } from '../../infrastructure';
import { CartResolver } from './cart.resolver';

@Module({
    imports: [ProxyModule],
    providers: [CartResolver],
})
export class CartModule { }
