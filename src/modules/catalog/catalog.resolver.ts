import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Observable } from 'rxjs';
import { Catalog, Product } from '../../types/graphql.schema'
import { CatalogMsg } from '../../utils/constants';
import { RabbitMQProxy } from '../../infrastructure/rabbitMQ/clientProxy';

@Resolver('Catalog')
export class CatalogResolver {
  constructor(private readonly clientProxy: RabbitMQProxy) { }
  private _clientProxyCatalog = this.clientProxy.clientProxyCatalog();

  @Query('getCatalog')
  getCatalog(): Observable<Catalog> {
    return this._clientProxyCatalog.send(CatalogMsg.GET_CATALOG, {});
  }

  @Query('getProduct')
  getProduct(@Args('id') id: string): Observable<Product> {
    return this._clientProxyCatalog.send(CatalogMsg.GET_PRODUCT, id);
  }

  @Mutation('addProduct')
  addProduct(
    @Args('id') id: string,
    @Args('name') name: string,
    @Args('stock') stock: number,
    @Args('price') price: number,
    @Args('category') category: string,
    @Args('color') color: string,
    @Args('genre') genre: boolean,
    @Args('description') description?: string,
    @Args('image') image?: string,
  ): Observable<Product> {
    return this._clientProxyCatalog.send(CatalogMsg.ADD, { id, name, stock, price, category, color, genre, description, image });
  }

  @Mutation('deleteProduct')
  deleteProduct(
    @Args('id') id: string,
    @Args('quantity') quantity: number,
  ): Observable<Product> {
    return this._clientProxyCatalog.send(CatalogMsg.DELETE, { id, quantity });
  }
}
