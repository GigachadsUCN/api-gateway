import { Module } from '@nestjs/common';
import { ProxyModule } from '../../infrastructure';
import { CatalogResolver } from './catalog.resolver';

@Module({
    imports: [ProxyModule],
    providers: [CatalogResolver],
})
export class CatalogModule { }
