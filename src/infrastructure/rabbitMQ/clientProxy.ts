import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ClientProxy,
  ClientProxyFactory,
  Transport,
} from '@nestjs/microservices';

import { RabbitMQ } from '../../utils/constants';

@Injectable()
export class RabbitMQProxy {
  constructor(private readonly config: ConfigService) { }

  clientProxyCart(): ClientProxy {
    return ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: this.config.get('AMQP_URL'),
        queue: RabbitMQ.CartQueue,
      },
    });
  }

  clientProxyCatalog(): ClientProxy {
    return ClientProxyFactory.create({
      transport: Transport.RMQ,
      options: {
        urls: this.config.get('AMQP_URL'),
        queue: RabbitMQ.CatalogQueue,
      },
    });
  }
}
