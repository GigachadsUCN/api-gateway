export enum RabbitMQ {
  CartQueue = 'cart',
  CatalogQueue = 'catalog',
}

export enum CartMsg {
  GET = 'GET_CART',
  ADD = 'ADD_ITEM',
  DELETE = 'DELETE_ITEM',
}

export enum CatalogMsg {
  GET_CATALOG = 'GET_CATALOG',
  GET_PRODUCT = 'GET_PRODUCT',
  ADD = 'ADD_PRODUCT',
  DELETE = 'DELETE_PRODUCT',
}
