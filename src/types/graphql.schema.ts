
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export abstract class IQuery {
    abstract getCart(id: string): Nullable<Cart> | Promise<Nullable<Cart>>;

    abstract getCatalog(): Nullable<Catalog> | Promise<Nullable<Catalog>>;

    abstract getProduct(id: string): Nullable<Product> | Promise<Nullable<Product>>;
}

export class Cart {
    id: string;
    items: CartItem[];
}

export class CartItem {
    productId: string;
    quantity: number;
}

export abstract class IMutation {
    abstract addCartItem(cartId: string, productId: string, quantity: number): Nullable<Cart> | Promise<Nullable<Cart>>;

    abstract deleteCartItem(cartId: string, productId: string, quantity: number): Nullable<Cart> | Promise<Nullable<Cart>>;

    abstract addProduct(id: string, name: string, stock: number, price: number, category: string, color: string, genre: boolean, description?: Nullable<string>, image?: Nullable<string>): Nullable<Product> | Promise<Nullable<Product>>;

    abstract deleteProduct(id: string, quantity: number): Nullable<Product> | Promise<Nullable<Product>>;
}

export class Product {
    id: string;
    name: string;
    stock: number;
    price: number;
    category: string;
    color: string;
    genre: boolean;
    description?: Nullable<string>;
    image?: Nullable<string>;
}

export class Catalog {
    products: Product[];
}

type Nullable<T> = T | null;
