import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { CartModule, CatalogModule } from './modules';
import { GraphQlModule } from './graphql/graphql.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    GraphQlModule,
    CartModule,
    CatalogModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
